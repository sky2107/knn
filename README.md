# Keras framework

> Question concerning Keras framework or libary

1. ImageDataGenerator all techniques
2. BatchNormalization mean = 0 and std = 1
3. framework or libary
4. ext of saved model and waht can be save weights, bias and ?
   * via json and yaml advantage
5. pass


> Guide for implementation with keras

1. tutorial concerning keras
2. keras documentation
3. configuration gpu and backend with tf => tensorflow
4. visualization concerning test examples
   * confusion matrix
   * [ANN Visualizer](https://github.com/Prodicode/ann-visualizer?utm_source=mybridge&utm_medium=blog&utm_campaign=read_more)
   * [keras visualisation](https://github.com/raghakot/keras-vis)
5. pass

> Code snippets

    code examples

#

> Checkout other projects

[Amazing ML Open Source of the Year](https://medium.mybridge.co/amazing-machine-learning-open-source-tools-projects-of-the-year-v-2019-95d772e4e985)

[python libaries 2019]((https://medium.mybridge.co/34-amazing-python-open-source-libraries-for-the-past-year-v-2019-93d6ee11aceb))


#

> <u> <b>Notes concerning keras</b> </u> <br>
> very for statistics looks like  <br>
> <br>
> it seems i do not need to be concerned about the connection <br>
> to the desnse fc layer like in pytorch <br>
> <br>
> another useful feature no transpose for tensors <br>
> color channel hast the last postion <br>
> <br>
> same thing with batch_size no stacking is necessary <br>
> <br>
> pass
# coding notes

> config json is important called => keras.json

#

> imports modules for keras <br>
> MUST have

    import numpy as np

    import keras 
    from keras import backend as K 
    from keras.models import Sequential
    from keras.layers import Activation
    from keras.layers.core import Dense, Flatten
    from keras.optimizers import Adam
    from keras.metrics import categorical_crossentropy

    from keras.preprocessing.image import ImageDataGenerator
    from keras.layers.normalization import BatchNormalization

    from keras.layers.convolutional import *